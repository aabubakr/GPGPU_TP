#version 410

uniform float lightIntensity; // I
uniform bool blinnPhong;
uniform float shininess;
uniform float eta;
uniform sampler2D shadowMap;
uniform bool metallic;   // new boolean to choose mettalic or non-mettalic material

float phong_exponent = shininess * 2; // phong exponent = 2*shininess

in vec4 eyeVector;  // V
in vec4 lightVector; // L
in vec4 vertColor; // C >> 4th component = 1
in vec4 vertNormal;
in vec4 lightSpace;

out vec4 fragColor;

float ComputeFresnel(float cosine_theta)
{
	float ci = sqrt( eta*eta - (1- cosine_theta*cosine_theta ) ); // sin^2() = 1 - cos^2() 
    float Fs = pow( (cosine_theta - ci) / (cosine_theta + ci), 2 );
    float Fp = pow( (eta*eta*cosine_theta - ci) / (eta*eta*cosine_theta + ci) , 2 );
    float F = (Fs + Fp)/2;
    return F;
}

// Fresnel with complex eta
float ComputeFresnelComplex(float n, float k, float c)
{
	float k2=k*k;
    float Fs_num = n*n + k2 - 2*n*c + c*c;
    float Fs_den = n*n + k2 + 2*n*c + c*c;
    float Fs = Fs_num/ Fs_den ;
     
    float Fp_num = (n*n + k2)*c*c - 2*n*c + 1;
    float Fp_den = (n*n + k2)*c*c + 2*n*c + 1;
    float Fp = Fp_num/ Fp_den ;
    float F = (Fs + Fp)/2;
    return F;
}

float Chi(float v)
{
    return v > 0 ? 1 : 0;
}

void main( void )
{
	 vec4 lightVector = normalize(lightVector);
	 vec4 eyeVector = normalize(eyeVector);
	 vec4 vertNormal = normalize(vertNormal);

     // Ambient
	 float ambient_reflection_coefficient = 0.7; // K_a
     vec4 C_ambient = ambient_reflection_coefficient * vertColor * lightIntensity;
     // Diffuse
     float diffuse_reflection_coefficient = 0.7; // K_d 
     vec4 C_diffuse = diffuse_reflection_coefficient * vertColor * lightIntensity * max( dot(vertNormal, lightVector) , 0.0) ;

     // complex number components (for metal)
     float n = 0.5;
     float k = 2.5;

     if (blinnPhong) 
     {
     	// Specular (blinn-phong)
     	vec4 H = normalize(eyeVector + lightVector);
     	// Remember, you don't need the angle theta itself, but cos(theta) directly (from dot product)
     	float cosine_theta = dot(H, lightVector);
     	float F = ComputeFresnel(cosine_theta);
     	if(metallic)
     	{
     		// eta is complex (n+ik)
        	F = ComputeFresnelComplex(n, k, cosine_theta) ;
    	}

     	vec4 C_specular = F * vertColor * lightIntensity * pow(max( dot(vertNormal, H), 0.0), phong_exponent);
     	vec4 phong_model = C_ambient + C_diffuse + C_specular;
     
     	fragColor = phong_model;
    }
    else 
    {
    	float roughness_alpha = 0.3;
    	vec4 H = normalize(eyeVector + lightVector); 
    	// theta_d >> angle between H and input ray == angle between H and out ray
    	float cosine_theta_d = dot(H, lightVector); 
    	float F_theta_d = ComputeFresnel(cosine_theta_d);
    	
    	if(metallic)
     	{
     		// eta is complex (n+ik)
        	F_theta_d = ComputeFresnelComplex(n, k, cosine_theta_d) ;
    	}

    	// theta_h >> angle between H and normal
    	float cosine_theta_h = dot(H, vertNormal);
    	float tan_sq_theta_h = (1-cosine_theta_h*cosine_theta_h)/(cosine_theta_h*cosine_theta_h);
    	float D = (Chi(cosine_theta_h)*roughness_alpha*roughness_alpha) / ( 3.141592 * pow(cosine_theta_h, 2) * pow(roughness_alpha*roughness_alpha+tan_sq_theta_h, 2) );

    	float cosine_theta_i = dot(vertNormal, lightVector);
    	float cosine_theta_o = dot(vertNormal, eyeVector);
    	float tan_sq_theta_i = (1-cosine_theta_i*cosine_theta_i)/(cosine_theta_i*cosine_theta_i);
    	float tan_sq_theta_o = (1-cosine_theta_o*cosine_theta_o)/(cosine_theta_o*cosine_theta_o);
    	float G_i = 2/(1 + sqrt(1 + roughness_alpha*roughness_alpha * tan_sq_theta_i) ); 
    	float G_o = 2/(1 + sqrt(1 + roughness_alpha*roughness_alpha * tan_sq_theta_o) ); 

    	vec4 C_specular_cook_torrance = ((F_theta_d*D*G_i*G_o) / (4*cosine_theta_i*cosine_theta_o)) * vertColor * lightIntensity * pow(max( dot(vertNormal, H), 0.0), phong_exponent);
        vec4 cook_torrance_model = C_ambient + C_diffuse + C_specular_cook_torrance;

    	fragColor = cook_torrance_model;
    }
}
