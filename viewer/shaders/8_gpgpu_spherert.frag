#version 410
#define M_PI 3.14159265358979323846

uniform mat4 mat_inverse;
uniform mat4 persp_inverse;
uniform sampler2D envMap;
uniform vec3 center;
uniform float radius;

uniform bool transparent;
uniform float shininess;
uniform float eta;

in vec4 position;
in vec2 textCoords;

out vec4 fragColor;


vec4 getColorFromEnvironment(in vec3 direction)
{
    // TODO
    //vec2 env_point = position.xy;
    vec3 base = texture2D( envMap, textCoords ).rgb;

    return vec4(base, 1);
    //return vec4(1);

}


bool raySphereIntersect(in vec3 start, in vec3 direction, out vec3 newPoint) {
	//Ray: starting point P, direction u
 	//Sphere : center C, radius r
 	bool is_intersection = false;
 	vec3 CP = start - center;  
 	float delta = pow(dot(direction, CP), 2) - dot(CP, CP) + radius*radius;
 	if (delta > 0) {is_intersection = true;}


 	return is_intersection;
    //return false;
}


void main(void)
{
    // Step 1: I need pixel coordinates. Division by w?
    vec4 worldPos = position;
    worldPos.z = 1; // near clipping plane
    worldPos = persp_inverse * worldPos;
    worldPos /= worldPos.w;
    worldPos.w = 0;
    worldPos = normalize(worldPos);
    // Step 2: ray direction:
    vec3 u = normalize((mat_inverse * worldPos).xyz); // direction
    vec3 eye = (mat_inverse * vec4(0, 0, 0, 1)).xyz;  // origin (pixel position ?)

    // TODO
    vec3 intersection;
    vec4 resultColor = vec4(0,0,0,1);
    if (raySphereIntersect(eye, u, intersection) )
    {
    	resultColor = vec4(1,1,1,1);
    }
    else
    {
    	resultColor = getColorFromEnvironment(u);
	}



    //vec4 resultColor = vec4(0,0,0,1);
    fragColor = resultColor;
}
